import requests
import json
import tornado.ioloop
import tornado.web
from tornado import gen
from tornado.httpclient import AsyncHTTPClient

def authorize_slack(token):
    def real_authorize_slack(function):
	@gen.coroutine
        def wrapper(self):
	    print token
 	    if (self.get_query_arguments('token')[0] != token):
		self.set_status(403)
		return
            yield function(self)
        return wrapper
    return real_authorize_slack 

class FlightAnalyzerDebugHandler(tornado.web.RequestHandler):

    @authorize_slack("6iG8jLRgeR40jYxBj4uHJKD6")
    @gen.coroutine
    def get(self):
        yield self.debug_log(self.get_query_arguments('text')[0])	

    @gen.coroutine
    def debug_log(self, log_id):
        print log_id
	self.write((yield AsyncHTTPClient().fetch("http://localhost:5009/api/debug/" + log_id)).body)
	print "done"

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'GET')



class FlightAnalysisGetter(tornado.web.RequestHandler):

    @authorize_slack("cSOGhG87N2zECLP0mupPdvth")
    @gen.coroutine
    def get(self):
	yield self.query_analysis(self.get_query_arguments('text')[0])	

    @gen.coroutine
    def query_analysis(self, log_id):
        print log_id
	self.write((yield AsyncHTTPClient().fetch("http://localhost:5009/api/flightlog/" + log_id)).body)
	print "done"

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'GET')


class HazardAnalyzerGetter(tornado.web.RequestHandler):

    @authorize_slack("ls9MDqTbvLvjaMvhmNh7KOt6")
    @gen.coroutine
    def get(self):
	yield self.query_analysis()

    @gen.coroutine
    def query_analysis(self):
	lng, lat, radius = self.get_query_arguments('text')[0].split(" ")
	geojson = {"type": "Feature", "properties": {"Radius": radius},"geometry": {"type": "Point", "coordinates":[lng, lat]}}
	self.write((yield AsyncHTTPClient().fetch("http://localhost:5001/api/hazards/", method="POST", headers={"Content-Type": 
"application/json"}, body=json.dumps(geojson))).body)

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'GET')


if __name__ == "__main__":

    app = tornado.web.Application([
        (r"/debug", FlightAnalyzerDebugHandler),
	(r"/analyze", FlightAnalysisGetter),
	(r"/hazards", HazardAnalyzerGetter)
    ])
    app.listen(21462)
    print('slack debugger ready..')
    tornado.ioloop.IOLoop.current().start()
